import kotlin.random.Random

val numberOfMatches = Random.nextInt(from = 10, until = 40)
var matchesPack = MutableList(numberOfMatches) { "|" }
var matchesLeft = matchesPack.size
var numberOfMatchesTakenByUser: Int = 1
var numberOfMatchesTakenByComputer: Int = 1
var userInput: Int = 0
var computerInput: Int = 0

fun main() {
    /*На столе лежит кучка спичек. При своем ходе игрок может
    взять спичек не меньше одной и не более чем вдвое больше,
    чем взял предыдущий игрок. Выигрывает тот, кто берет последнюю
    спичку.*/

    println("Let the game begin! Total number of matches: $matchesLeft")

    if(matchesPack.isNotEmpty()) {
        for(match in 0..matchesPack.size) {

            /* выбрать спички
            сравнить с дефолтом
            если больше - вернуть к выбору
            если меньше - пропустить дальше
            * */

            askUserInput()
            compareUserInputWithPreviousComputerInput()

            if(matchesPack.size == userInput) {
                println("You win!")
                break
            }
            nextTurn(input = userInput)

            println("Dear Computer, enter the number of matches you want to pick: ")
            askComputerInput()
            compareComputerInputWithPreviousUserInput()
            println(computerInput)
            if(matchesPack.size == computerInput) {
                println("Computer wins!")
                break
            }
            else {
                println("Your virtual opponent has picked: $computerInput matches")
            }
            nextTurn(input = computerInput)
        }
    }
}

fun nextTurn(input: Int) {
    if(input == 0) {
        println("Nice try, but you can't pick no matches, please pick some")
        askUserInput()
    }
    else if(input < 0) {
        println("You can't add any matches, sorry :)")
        askUserInput()
    }
    else if(input > matchesPack.size) {
        println("There are only " + matchesPack.size + " matches left, please enter a correct number")
        askUserInput()
    }
    else {
        matchesLeft = matchesPack.size - input
        //println("$input matches have been picked")
        matchesPack = MutableList(matchesLeft) {"|"}
        println("Total number of matches left: " + matchesPack.size)
    }
}

fun askUserInput(): Int {
    println("Dear User, enter the number of matches you want to pick: ")
    userInput = readLine()!!.toInt()
    return userInput
}

fun askComputerInput(): Int {
    computerInput = Random.nextInt(from = 1, until = matchesLeft+1)

    return computerInput
}

fun compareUserInputWithPreviousComputerInput() {

    while (userInput > numberOfMatchesTakenByComputer * 2){
        println("You can't take this many. Leave some on the table.")
        askUserInput()
    }
    numberOfMatchesTakenByUser = userInput
}

fun compareComputerInputWithPreviousUserInput() {

    while (computerInput > numberOfMatchesTakenByUser * 2){
        //println("Dear Computer, you can't take this many. Leave some on the table.")
        askComputerInput()
    }
    numberOfMatchesTakenByComputer = computerInput
}